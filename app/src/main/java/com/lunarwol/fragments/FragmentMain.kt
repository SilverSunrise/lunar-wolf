package com.lunarwol.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import com.lunarwol.R
import com.lunarwol.data.WolfCard
import kotlinx.android.synthetic.main.main_fragment.*

class FragmentMain : Fragment() {
    override fun onCreateView(

        inflater: LayoutInflater, container: ViewGroup?,

        savedInstanceState: Bundle?

    ): View? {
        val v = inflater.inflate(R.layout.main_fragment, container, false)

        val buttonWolfChartIcon: MutableList<Int> = dataList(v)

        buttonWolfChartIcon.addAll(buttonWolfChartIcon)
        buttonWolfChartIcon.shuffle()

        wolfCards = buttonWolfCards.indices.map { number ->
            WolfCard(buttonWolfChartIcon[number])
        }

        buttonWolfCards.forEachIndexed { indexCard, wolfsCard ->

            wolfsCard.setOnClickListener {

                renewCaseWolfCards(indexCard)

                renewSightWolfCards()

                amountOfSnap += 1

                tvShowSnaps.text = "Snaps: $amountOfSnap"

            }
        }

        relaunchingPlay(v)

        renewSightWolfCards()
        return v
    }

    private fun dataList(v: View): MutableList<Int> {
        val buttonWolfChartIcon: MutableList<Int> = mutableListOf(
            R.drawable.card_wolf_3,
            R.drawable.card_wolf_5,
            R.drawable.card_wolf_8,
            R.drawable.card_wolf_1,
            R.drawable.card_wolf_7,
            R.drawable.card_wolf_4,
            R.drawable.card_wolf_6,
            R.drawable.card_wolf_2
        )

        buttonWolfCards = listOf(
            v.findViewById(R.id.wolfsChart3),
            v.findViewById(R.id.wolfsChart15),
            v.findViewById(R.id.wolfsChart12),
            v.findViewById(R.id.wolfsChart4),
            v.findViewById(R.id.wolfsChart1),
            v.findViewById(R.id.wolfsChart16),
            v.findViewById(R.id.wolfsChart6),
            v.findViewById(R.id.wolfsChart14),
            v.findViewById(R.id.wolfsChart10),
            v.findViewById(R.id.wolfsChart9),
            v.findViewById(R.id.wolfsChart2),
            v.findViewById(R.id.wolfsChart7),
            v.findViewById(R.id.wolfsChart5),
            v.findViewById(R.id.wolfsChart11),
            v.findViewById(R.id.wolfsChart8),
            v.findViewById(R.id.wolfsChart13)
        )
        return buttonWolfChartIcon
    }

    companion object {
        private lateinit var buttonWolfCards: List<ImageButton>

        private var onePointer: Int? = null

        private var amountOfSnap: Int = 0

        private lateinit var wolfCards: List<WolfCard>

        private var remainderMatch: Int = 8

    }

    private fun renewCaseWolfCards(cardsPointer: Int) {

        if (wolfCards[cardsPointer].isTurned) {

            amountOfSnap -= 1

            return

        }
        onePointer = if (onePointer == null) {

            cardsTurn()

            cardsPointer
        } else {

            isWolfsCardsMatch(onePointer!!, cardsPointer)

            null
        }

        wolfCards[cardsPointer].isTurned = !wolfCards[cardsPointer].isTurned
    }

    private fun cardsTurn() {

        for (mark in wolfCards) {

            if (!mark.isMatch) {

                mark.isTurned = false

            }
        }

    }

    private fun relaunchingPlay(v: View) {
        val buttonRelaunching = v.findViewById(R.id.buttonRelaunch) as Button
        buttonRelaunching.setOnClickListener {

            fragmentManager?.beginTransaction()?.replace(R.id.fragmentContainer, FragmentMain())

                ?.commit()

            remainderMatch = 0

            amountOfSnap = 0

        }
    }

    private fun renewSightWolfCards() {

        wolfCards.forEachIndexed { time, i ->

            if (i.isMatch) {

                buttonWolfCards[time].alpha = 0.45f
            }

            buttonWolfCards[time].setImageResource(if (i.isTurned) i.look else R.drawable.card_back)
        }
    }

    private fun isWolfsCardsMatch(forwardWolfsCard: Int, secondaryWolfsCard: Int) {
        if (wolfCards[forwardWolfsCard].look == wolfCards[secondaryWolfsCard].look) {

            wolfCards[forwardWolfsCard].isMatch = true

            wolfCards[secondaryWolfsCard].isMatch = true

            remainderMatch -= 1

            if (remainderMatch == 0) {

                showWin.animate().apply {

                    showWin.visibility = View.VISIBLE
                    duration = 600
                    alpha(0.75f)
                }.withEndAction {

                    showWin.animate().apply {

                        duration = 600

                        alpha(1.05f)
                    }

                }.start()

                buttonRelaunch.visibility = View.VISIBLE

            }

            showRestMatch.text = "Rest match: $remainderMatch"

        }
    }

}