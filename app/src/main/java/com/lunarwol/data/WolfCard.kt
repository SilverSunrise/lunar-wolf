package com.lunarwol.data

data class WolfCard(
    var look: Int,
    var isTurned: Boolean = false,
    var isMatch: Boolean = false
)
