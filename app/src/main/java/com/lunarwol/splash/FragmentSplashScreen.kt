package com.lunarwol.splash

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.lunarwol.fragments.FragmentMain
import com.lunarwol.R

class FragmentSplashScreen : Fragment() {

    companion object {

        private const val SPLASH_SCREEN_TIME: Long = 4800

    }

    override fun onCreateView(

        inflater: LayoutInflater, container: ViewGroup?,

        savedInstanceState: Bundle?

    ): View? {

        val v = inflater.inflate(R.layout.splash_screen_fragment, container, false)

        Handler().postDelayed({

            fragmentManager?.beginTransaction()?.replace(R.id.fragmentContainer, FragmentMain())

                ?.commit()

        },
            SPLASH_SCREEN_TIME)

        return v
    }


}