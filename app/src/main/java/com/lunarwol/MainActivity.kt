package com.lunarwol

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.lunarwol.splash.FragmentSplashScreen

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val splashScreenFragment = FragmentSplashScreen()

        supportFragmentManager.beginTransaction()

            .replace(R.id.fragmentContainer, splashScreenFragment)

            .commit()

    }
}